class Lab2 
{
	public static void main(String[] args){
		String name="Rinkun";
		double physics=50, english=45, maths=55;
		double Avg, Total;

		Total = physics + english + maths;
		Avg=Total/3;

		System.out.println("Total marks: "+Total);
		System.out.println("Average marks: "+Avg);
		
		if(Avg>=60){
			System.out.println("1st Class");
		}
		else if(Avg>=50){
			System.out.println("2nd Class");
		}
		else if(Avg>=35){
			System.out.println("Pass");
		}
		else{
			System.out.println("Failed");
		}
	}
}